#  3 - Administració de Sistemes Operatius Linux

Taula de continguts

[TOC]

## 3.1 - Serveis: SystemD

### 3.1.1 La polèmica amb SysV

Tradicionalment els sistemes Linux han gestionat els seus serveis amb **SysV**. Eren un conjunt d'scripts a ```/etc/init.d``` en distribucions basades en Debian i a ```/etc/rc.d/init.d``` en les basades en RedHat. Podíem crear els nostres propis gestors de serveis *SysV* fent un script com [aquest](https://www.cyberciti.biz/tips/linux-write-sys-v-init-script-to-start-stop-service.html). Executant aquests scripts amb els paràmetres *start*, *stop*, *status*, *restart* i *reload* podíem iniciar, aturar, etc... el servei corresponent. Per exemple el següent script iniciaria el servidor apache:

`/etc/init.d/httpd start`

Per tant el sistema tradicional *SysV* es basava en els principis de simplicitat i de gestionar només l'arrencada i aturada de serveis correctament dins d'uns nivells establerts, els [runlevels](https://learn.adafruit.com/running-programs-automatically-on-your-tiny-computer/sysv-init-runlevels) que el definien l'ordre. 

Un bon dia de 2010 un noi a RedHat va decidir que era millor canviar completament la manera com s'inicien els serveis al linux, principalment per intentar aportar una millor definició de dependències entre serveisi permetre també més execucions en paral.lel i concurrents, especialment durant l'arrencada del sistema operatiu. Aquí teniu a Lennart Poettering en una entrevista:

<iframe src="//commons.wikimedia.org/wiki/File:LCA_TV-_Lennart_Poettering.webm?embedplayer=yes" width="640" height="480" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

Aviat aquest nous sitema, el **SystemD** va ser adaptat per les distribucions basades en RedHat i va crear grans debats dins la comunitat Linux, especialment a Debian, on inclús van arribar a enemistar-se'n els seus mantenidors arran de la polèmica sobre si abandonar SysV pel SystemD o no. Així que ara existeix la distribució Debian (amb el nou SystemD) i la [Debuan](https://devuan.org/) que segueix resistint-se a abandonar el SysV.

Els defensors del *SysV* es basen en la filosofia original de Linux "fes una cosa i fes-la bé". Aquest nou *SystemD* crea múltiples binaris que miren de fer moltes més coses de les que tradicionalment feina el *SysV*. Això fa que el sistema passi a ser molt més complex ja que ara ja no gestiona l'arrencada i aturada de serveis solsament sinó que ens trobem que fa control d'energia, gestió de dispositius, gestió de logins, particions GPT, ...

### 3.1.2 SystemD

Per a gestionar els serveis ara ho farem amb l'ordre **systemctl**. Aquesta ordre buscarà el fitxer de configuració del servei per tal de realitzar-ne l'acció corresponent que s'hi indiqui. Tots els serveis poden tenir dependències i això farà que no només s'iniciï el nostre servei sinó també els associats.

#### 3.1.2.1 Fitxers de configuració: Service units

Els fitxers de configuració de serveis de systemd tenen extensió *.service* i es troben en tres llocs:

-  /usr/lib/systemd/system: Aquí hi ha els que venen amb els paquets que intal.lem (.deb, .rpm,...)
- /run/systemd/system: Aquí hi ha els que es creen en inicial el sistema. Són més prioritaris que els anterior.
- /etc/systemd/system: Aquí és on com administrador podem afegir-hi les nostres configuracions de serveis, que seran més prioritaris que tots els anteriors.

Podem veure que el systemd està corrent al nostre sistema:

```
ps -eaf | grep [s]ystemd
```

I en realitat veureu que té diversos serveis que està controlant: dispositius (udev), login, sessions... Veureu que n'hi ha un que té el PID 1, el principal. D'aquí deriven tota la resta de processos, que haurà arrencat aquest servei en pendre el control durant l'arrencada. Podem analitzar l'arrencada del nostre sistema:

```
systemd-analyze
```

I inclús veure-ho amb més detall:

```
systemd-analyze blame
```

Podem veure la cadena d'inici de serveis. Veureu que no només hi ha serveis (.service), també hi haurà altres unitats com targets, mounts, sockets, devices... cadascún tindrá una funció específica, no només hi haurà serveis genèrics que podrem iniciar o aturar.

Podem veure totes les unitats disponibles amb:

```
systemctl list-unit-files
```

I les que s'estan executant:

```
systemctl list-units
```

O les que han fallat en iniciar-se:

```
systemctl --failed
```

Per exemple podem comprovar si el servei de cron està funcionant: systemctl is-enabled crond.service o veure-ho amb més detall amb:

```
systemctl status crond.service
```

També podem veure tots els serveis que necessita un servei per tal de ser iniciat amb el temps que han trigat en iniciar-se:

```
systemd-analyze critical-chain httpd.service
```

I veure'n les dependències:

```
systemctl list-dependencies httpd.service
```



Per actuar amb els serveis podem llistar els que hi han disponibles i executar una de les accions que volguem:

```
systemctl list-unit-files --type=service
```

```
systemctl start httpd.service
systemctl restart httpd.service
systemctl stop httpd.service
systemctl reload httpd.service
systemctl status httpd.service
```

I per a fer que un servei s'iniciï sempre durant l'arrencada ho farem amb *enable* o *disable*. 

##### Fitxer de configuració de servei

Imaginem que hem creat un servidor web mitjançant python desde la línia d'ordres:

```python -m SimpleHTTPServer```

Podeu comprovar amb un navegador que si accedir al port per defecte (http://localhost:8000/) hi veureu els fitxers del directori on heu arrencar el servidor.

Ara el que volem és poder controlar aquest servidor mitjançant un servei nostre que crearem. Aquest servei podrà iniciar-se o aturar-se i establir-ne l'inici durant l'arrencada del sistema operatiu.

Podem crear un fitxer a /etc/systemd/system/ anomenat webserver.service

```
[Unit]
Description=El meu servidor web

[Service]
ExecStart=/usr/bin/python -m SimpleHTTPServer

[Install]
WantedBy=multi-user.target
```

Una vegada creat si volem provar si funciona primer caldrà recarregar els serveis:

``` systemctl daemon-reload```

i ara ja tindrem disponible el servei. Prove d'iniciar-lo, aturar-lo, etc... Teniu moltes més [opcions de configuració](https://www.shellhacks.com/systemd-service-file-example/).

Podeu veure els logs del servei amb *journalctl -u webserver*. Més endavant tractarem el tema dels logs.

#### 3.1.2.2 Nivells: Target units

Antigament hi havia els *runlevels* del 0 al 6 al SysV que en passar a SystemD són els *Targets*. Aquests fitxers porten com extensió *.target*. 

La funció dels targets no és una altra que agrupar una sèrie de *service units* amb les seves dependències. Per exemple existeix el *graphical.target* que pot arrencar serveis com el *gdm.service*, *accounts-daemon.service*, etc... i ho farà en ordre i amb les sesves dependències.

Aquí tenim una relació entre els nivells que existien antigament (SysV) i els actuals targets de SystemD:

| Runlevel | Target Units                            | Description                               |
| -------- | --------------------------------------- | ----------------------------------------- |
| `0`      | `runlevel0.target`, `poweroff.target`   | Shut down and power off the system.       |
| `1`      | `runlevel1.target`, `rescue.target`     | Set up a rescue shell.                    |
| `2`      | `runlevel2.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `3`      | `runlevel3.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `4`      | `runlevel4.target`, `multi-user.target` | Set up a non-graphical multi-user system. |
| `5`      | `runlevel5.target`, `graphical.target`  | Set up a graphical multi-user system.     |
| `6`      | `runlevel6.target`, `reboot.target`     | Shut down and reboot the system.          |

Podem veure el target default que establirà el sistema en arrencar amb:

```
~]$ systemctl get-default
graphical.target
```

LListar tots els targets actius:

```
systemctl list-units --type target
```

Llistar tots els targets disponibles:

```
systemctl list-units --type target --all
```

Llistar tots els fitxers associats amb els targets:

```
systemctl list-unit-files --type target
```

Canviar el default target (que es carregarà en la propera arrencada):

```
systemctl set-default graphical.target
```

També podem canviar-ho en la sessió actual:

```
systemctl isolate graphical.target
```

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-targets](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-targets)

## 3.2 - Logs: Journal

Tradicionalment hem accedit als logs en fitxers de text en format **syslog** a /var/log/ però amb l'arribada del *systemd* també va arribar el **journald**. El Journald emmagatzema els logs en format binari. Per accedir-hi farem ús de l'ordre *journalctl*. Teniu moltíssimes [opcions per a filtrar els logs](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs) però algunes de les més útils per nosaltres ara són:

Veure logs d'un servei concret:

```journalctl -u <servei>```

Gestionar els logs de les arrencades del sistema:

```journalctl -b
journalctl -b
journalctl --list-boots
journalctl -b -1
```

O veure per dates:

```
journalctl --since "2015-01-10 17:15:00"
```

Veure missatges del kernel:

```
journalctl -k
journalctl -k -b -5
```

O veure els més importants segons l'escala de prioritats:

- 0: emerg
- 1: alert
- 2: crit
- 3: err
- 4: warning
- 5: notice
- 6: info
- 7: debug

```
journalctl -p err -b
```

## 3.3 - Gestió de xarxes



## 3.4 - Tallafocs: FirewallD