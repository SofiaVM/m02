# E1. Exercici 9. Processos

## Introducció


## Continguts

Hi ha diferents estats en els que es troben els processos i que podem veure amb la comanda **ps**:

### PROCESS STATE CODES

-   R  running or runnable (on run queue)
-   D  uninterruptible sleep (usually IO)
-   S  interruptible sleep (waiting for an event to complete)
-   Z  defunct/zombie, terminated but not reaped by its parent
-   T  stopped, either by a job control signal or because it is being traced

Per veure un procés en running (dins la cpu) podem fer servir el programa **stress** (dnf install stress): sudo stress --cpu 4 -v --timeout 20s

Per veure estat de procés: **ps -el | grep 'nom del procés'**. En aquest cas el nom del procés és abrt-applet

```
F S UID        PID  PPID  C PRI  NI ADDR SZ WCHAN  STIME TTY          TIME CMD
0 S darta     2296  1971  0  80   0 - 162756 poll_s 22:00 tty2    00:00:00 abrt-applet
```

Fixeu-vos que l'estat és la segona columna (S) i que després de l'usuari, a les columnes 4 i 5 hi tenim l'ID de procés (PID) i l'ID del procés pare (PPID)

Per veure diferents estats farem ús d'aquest script python que anomenarem hyperloop.py. Creeu-lo i copieu aquest codi dins:

```
import time
i=1
while i<9999:
    print(i)
    i=i+1
    time.sleep(1)
```

I amb un procés que estiguem executant podem:
- Aturar-lo amb CTRL+Z
- Recuperar-lo en segon pla (backgroud) amb la comanda **bg**. Això farà que es segueixi executant però no veurem la sortida per pantalla.
- Recuperar-lo en primer pla amb la comadna **fg**. Això farà que recuperem la sortida del programa per pantalla.


Amb aquesta informació i els fiters continguts en aquesta mateixa carpeta (zombie.c y a.out) responeu les segûents preguntes:

## Entrega

1. ** Expliqueu què fa l'script de python que usarem i quan acabarà l'execució**


2. ** Executeu ara en un terminal l'script amb python hyperloop.py. Mostreu el procés amb la comanda: ps -elf | grep 'loop' i l'estat en el que es troba.**


3. ** Mentre s'està executant premeu CTRL+C. Si busqueu el proces amb la comanda: ps -elf | grep 'loop', hi és? Expliqueu què ha passat**


4. ** Torneu a executar en un terminal l'script. Mentre s'estigui executant premeu CTRL+Z. En quin estat es troba ara? Mostreu el resultat de la comanda ps**


5. ** Recuperem en primer pla el procés python amb la comanda fg. S'ha seguit executant tota aquesta estona? Expliqueu perquè.**


6. ** Torneu a prémer CTRL+Z amb el procés python funcionant. Ara recuperem-ne l'execució però en segon pla amb la comanda bg. En quin estat es troba ara el procés? Mostreu el resultat de la comanda ps**


7. ** Ara recuperem-ne l'execució en primer pla amb la comanda fg. S'ha seguit executant tota aquesta estona? Expliqueu perquè i finalitzeu l'execució de l'script definitivament amb CTRL+C.**


Ara executarem un codi c compilat (un executable) que crearà un procés pare i un fill. El procés fill finalitzarà però el procés pare es quedarà esperant. Això provocarà una situació en la qual el procés fill quedarà en estat zombie. El fitxer amb el codi font en llenguatge C el teniu al fitxer zombie.c que, usant el compilador gcc (gcc zombie.c) de C s'ha generat el fitxer binari a.out que executarem amb: bash a.out. Aquest executable finalitzarà al cap de 100 segons, ho heu de tenir en compte a l'hora de realitzar les següents qûestions.

8. ** Mentre s'estigui executant comproveu que apareixen dos processos a.out amb la comanda ps -elf | grep 'a.out'. Mostreu el resultat de la comanda i indiqueu en quin estat es troben**


9. ** Quin dels dos processos a.out que surten a la pregunta anterior és el pare i quin el fill? Com ho heu deduît? **

10. ** En una finestra de terminal inicieu una descàrrega de un dels dvd iso de debian i realitzeu les següents operacions en una altra finestra. Poseu aquí les ordres que executeu. **
- Ordre per iniciar la descàrrega i estat del procés de descàrrega:
- Ordre per veure l'estat de la descàrrega:
- Combinació de tecles per aturar la descàrrega i estat del procés de descàrrega:
- Ordre per recuperar l'execució de la descàrrega en segon pla i estat del procés de descàrrega:
- Ordre per recuperar l'execució de la descàrrega en primer pla i estat del procés de descàrrega:

